package com.example.bonosapp

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.net.HttpURLConnection
import java.net.URL
import android.os.AsyncTask
import java.lang.Exception
import java.time.LocalDateTime


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         val discoverBlu = BluetoothAdapter.getDefaultAdapter()
        if(discoverBlu == null){
            btnStart.text = "su dispositivo es una *@#/%$"
        }
        btnStart.setOnClickListener {
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
            startActivity(discoverableIntent)

        }
        btnLista.setOnClickListener {
            val instanciaCalendario  =LocalDateTime.now()
            val diaActual = instanciaCalendario.dayOfMonth.toString() + "-" + (instanciaCalendario.monthValue - 1).toString() + "-" + instanciaCalendario.year.toString()
            btnLista.text = "consultando..."
            AsyncTask.execute {
                val connection = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + diaActual +"/students/201617597").openConnection() as HttpURLConnection
                try {
                    val data = connection.inputStream.bufferedReader().readText()
                    btnLista.text = "Estas en la lista!"
                }
                catch (e: Exception){
                    btnLista.text = "Aun no estas en la lista !!"

                }finally{
                    connection.disconnect()
                }
            }
        }
    }
}
